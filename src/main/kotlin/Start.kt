import com.google.protobuf.ByteString
import com.ibm.etcd.client.EtcdClient
import com.ibm.etcd.client.KvStoreClient


fun main() {
    val client: KvStoreClient = EtcdClient.forEndpoint("localhost", 2379).withPlainText().build()


    val kvClient = client.kvClient
    val leaseClient = client.leaseClient
    val lockClient = client.lockClient

    val value: ByteString = ByteString.copyFromUtf8("value")
    val key: ByteString = ByteString.copyFromUtf8("key")
    val result = kvClient.put(key, value).sync()

    val get  = kvClient.get(key).sync()
    println(get.kvsList[0].value.toStringUtf8())
    print("juhuuu")
}